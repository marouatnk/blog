# Blog 

Un blog réalisé en PHP en utilisant le framework Symfony 4.3 et  PDO pour apprendre la gestion de base de données.
## Conception :

* Réalisation de diagramme de Use Case : 

En réalisant un diagramme de Use Case j'ai pu mettre en valeur les fonctionnalités de mon blog .

 <img src="public/images/blogUseCase.png" width="800" height="500" />

* Réalisation de diagramme de Classe : 

 <img src="public/images/class.png" width="800" height="500" />


* Réalisation des maquette fonctionnelles :

Le maquettage de mon projet était facile à faire car j'avais qu'une seule page html à réaliser et dans cette page y'avait pas  trop d'éléments à manipuler .


 <img src="public/images/landingPage.png" width="800" height="500" />
 <img src="public/images/createArticle.png" width="800" height="500" />
 <img src="public/images/modDel.png" width="800" height="500" />
 <img src="public/images/allArticles.png" width="800" height="500" />

## Oraganisation : 
Afin d'organiser mon travail et d'avoir un plan à suivre j'ai utiliser des fonctionnalités de Trello : 


chaque semaine j'ai créer une nouvelle "Sprint" pour préciser ce ques je voulais faire pendant toute la semaine . 
puis dans chaque liste j'ai détaillé un peu chaque fonctionalité en créant des sous taches .
Puis pour organiser les taches j'ai créee des labels  : les taches que je vais faire / Les taches qui je suis en train de faire / Les taches à faire .

Voici un exemple de mon deuxième et dernier sprint :

 <img src="public/images/trello.png" width="800" height="500" />




## Authored by  :

#### @marouatnk a.k.a Me ^_^



<h5>Le projet est libre de droits. </h5>
