<?php

namespace App\Repository;

//use App\Entity\Article;

class Pdo
{
    private $pdo;

    public function __construct() {
        $this->pdo = new \PDO(
            "mysql:host=" . $_ENV["DATABASE_HOST"] . ";dbname=" . $_ENV["DATABASE_NAME"],
            $_ENV["DATABASE_USERNAME"],
            $_ENV["DATABASE_PASSWORD"],
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]
        );
    }

    public function getPdo(): \PDO {
        return $this->pdo;
    }
}