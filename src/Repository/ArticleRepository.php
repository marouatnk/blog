<?php

namespace App\Repository;
use App\Repository\Pdo;
use App\Entity\Article;
use DateTime;

class ArticleRepository {
    /**
     * Find all articles in our database 'blog' 
     * converting them to instances of Article
     * @return Article[] : articles in our bd
     */

        public function findAll() : array{

            $pdo = new Pdo();
            $articles=[];
            $query = $pdo->getPdo()->prepare("SELECT * FROM article");
            $query->execute();
            foreach ($query->fetchAll() as $line) {
                
                $articles[] = $this->sqlToArticle($line);
            }

            return $articles;
        }


        public function add(Article $article): void
        {
            $pdo = new Pdo();
            $query = $pdo->getPdo()->prepare("INSERT INTO article (title_article,date_article,content_article,author_article) 
            VALUES (:title_article, :date_article, :content_article, :author_article)");
           
            $query->bindValue(":title_article", $article->getTitle(), \PDO::PARAM_STR);
            $query->bindValue(":date_article", $article->getDate()->format('m-d-Y'));
            dump($article->getDate()->format('m-d-Y'));
            $query->bindValue(":content_article", $article->getContent(), \PDO::PARAM_STR);
            $query->bindValue(":author_article", $article->getAuthor(), \PDO::PARAM_STR);

            $query->execute();
    
            $article->setId(intval($pdo->getPdo()->lastInsertId()));    
        }

        public function lastId(): Article {

            $pdo = new Pdo();
            $query = $pdo->getPdo()->prepare("SELECT * FROM article ORDER BY id_article DESC LIMIT 1");
            $query->execute();
    
            if ($line = $query->fetch()) {
    
                return $this->sqlToArticle($line);
            }
        }
        /**
         * updating my article 
         */
        public function update(Article $article): void
        {
           $pdo = new Pdo();
           $query = $pdo->getPdo()->prepare("UPDATE article SET title_article=:title_article, author_article=:author_article, date_article=:date_article, content_article=:content_article WHERE id_article=:id_article");
           $query->bindValue(":title_article", $article->getTitle());
           $query->bindValue(":author_article", $article->getAuthor());
           $query->bindValue(":date_article", $article->getDate()->format('Y-m-d'));
           $query->bindValue(":content_article", $article->getContent());
           $query->bindValue(":id_article", $article->getId());
           $query->execute();
        }
    
        public function remove(Article $article): void{
          
           $pdo = new Pdo();
           $query = $pdo->getPdo()->prepare("DELETE FROM article WHERE id_article =:id_article");
           $query->bindValue(":id_article", $article->getId() );
           $query->execute();

        }

        public function findById(int $id): ?Article {
            $pdo = new Pdo();
            $query = $pdo->getPdo()->prepare('SELECT * FROM article WHERE id_article=:id_article');
            $query->bindValue(':id_article', $id, \PDO::PARAM_INT);

            $query->execute();
            $line = $query->fetch();
            if($line) {
                return $this->sqlToArticle($line);
            }
           
            return null;
    
        }

    public function search(string $words)
    {

        $article = [];
        $pdo = new Pdo();

        $query = $pdo->getPdo()->prepare("SELECT * FROM article WHERE title_article LIKE :words");
        $query->bindValue(":words", '%'.$words.'%');
        $query->execute();

        foreach ($query->fetchAll() as $key => $value) {
            $article[]= $this->sqlToArticle($value);
        }
        return $article;
    }

    
    /**
     * A method that transfers a PDO result into an instance of Article
     * We create this method to prevent repetition in each "find" function  :( find function / lastId function/findLastRow function)
     */

    private function sqlToArticle(array $line):Article {

        return new Article($line['title_article'], 
                new \DateTime( $line["date_article"]),
                $line['content_article'], 
                $line['author_article'],
                $line['id_article']);
    }


}
