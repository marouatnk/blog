<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;   
use App\Entity\Article;


class ArticleController extends AbstractController {
  
    /**
  
     * @Route("/all-articles", name="all_articles")
     */
    public function index(ArticleRepository $repo) { //On injecte notre DAO
        
        $articles = $repo->findAll();
        // dump($articles);
        return $this->render('all-articles.html.twig', [
            'articles' => $articles
        ]);
    }
       /**
  
     * @Route("/article/{id}", name="one_article")
     */
    public function oneArticle(int $id) { //On peut ensuite ajouter le paramètre en argument de la méthode en mettant le même nom que dans les accolades de la route
        $repo = new ArticleRepository();
        $article = $repo->findById($id);
        return $this->render('show-article.html.twig', [
            'article' => $article
        ]);
    }
     /**
     * @Route("/add-article", name="add_article")
     */

    public function articleForm(Request $request, ArticleRepository $repo){

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $repo->add($article);
            return $this->redirectToRoute('all_articles');
        }
        return $this->render('add-article.html.twig',[
            "formArticle" => $form->createView()
        ]);
    }
    /**
     * @Route("/delete-article/{id}", name="delete_article")
     */

    public function remove(int $id, ArticleRepository $repo){
            
        $article = $repo->findById($id);
        dump($article);
        $deleteArticle = $repo->remove($article);    
        return $this->redirectToRoute("all_articles");
    }

/**
     * @Route("/edit-article/{id}", name="edit_article")
     */

    public function edit(int $id, Request $request, ArticleRepository $repo)
    {
        $oldArticle = $repo->findById($id);
        $form = $this->createForm(ArticleType::class, $oldArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $oldArticle = $form->getData();
            $repo->update($oldArticle);
            dd($repo->update($oldArticle));
            dump($oldArticle);
            return $this->redirectToRoute('all_articles');

        }
        return $this->render("edit-article.html.twig", [

            "modif"=>$form->createView()
        ]);

    }
      /**
     * @Route("/search-article", name="search_article")
     */
    public function searchArticle(Request $request, ArticleRepository $repo)
    {
        
        $articles = $repo->search($request->get('search'));
        return $this->render('search.html.twig',[
            "articles" => $articles
        ]);

    }





}



