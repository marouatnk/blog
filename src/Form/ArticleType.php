<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use App\Entity\Article;


class ArticleType extends AbstractType {

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
            [
                'attr' => [
                    'placeholder' =>" Title of the Article"
                ]
            ])

            ->add('date', DateTimeType::class, 
                    [
                      'widget' => 'single_text', 
                      'format' => 'mm-dd-yyyy'
                    ]
                    )

            ->add('content', TextareaType::class,
            [
                'attr' => 
                [
                    'placeholder' =>" Content of the Article"
                ]
            ])
             ->add('author', TextType::class,
            [
                'attr' => 
                [
                    'placeholder' =>" Author of the Article"
                 ]
             ]);
             
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=>Article::class
        ]);
    }
    
    
}
