<?php

namespace App\Entity;
use DateTime;
class Article {
   
    private $id;
    private $title;
    private $date;
    private $content;
    private $author;

    public function __construct(string $title='', \DateTime $date= null  , string $content='', string $author='', int $id = null) {
        
        $this->id = $id;
        $this->title = $title;
        $this->date= $date;
        $this->content = $content;
        $this->author = $author;

    }

   
    public function getId(): INT{

        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }


    public function getTitle(): string{

        return $this->title;
    }

    public function setTitle(string $title){

        $this->title = $title;
        return $this;
    }

    public function getContent(): string{

        return $this->content;
    }

    public function setContent(string $content){

        $this->content = $content;
        return $this;
    }

    public function getDate():?DateTime{
        return $this->date;
    }

    public function setDate(\DateTime $date){
        $this->date = $date;
        return $this;
    }

    public function getAuthor(): string{
        return $this->author;
    }

    public function setAuthor(string $author){

        $this->author = $author;
        return $this;
    }
     
}