-- Delete the database if it exists
    DROP DATABASE IF EXISTS blog;

-- Create the database
    CREATE DATABASE blog;

-- we select the database to use it
    USE blog;

--create table called article 
    CREATE TABLE article( 
       id_article INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
       title_article VARCHAR(64) NOT NULL,
       date_article DATETIME,
       content_article VARCHAR(15000),
       author_article VARCHAR(70)
    ); 
     
--create table called home 
    CREATE TABLE home( 
       id_home INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
       title_home VARCHAR(64) NOT NULL,
       img_home VARCHAR(150),
       content_home VARCHAR(15000) 
    );
-- inserting an article to our table article to test it 



 INSERT INTO article (

        title_article,
        date_article,
        content_article,
        author_article
)
 VALUES (
        'Is Blockchain Technology the New Internet ?',
        NOW(),
        'The blockchain is an undeniably ingenious invention – the brainchild of a person or group of people known by the pseudonym, Satoshi Nakamoto. But since then, it has evolved into something greater, and the main question every single person is asking is: What is Blockchain?
        By allowing digital information to be distributed but not copied, blockchain technology created the backbone of a new type of internet.' ,
        'Maroua '
 );
 INSERT INTO article (

        title_article,
        date_article,
        content_article,
        author_article
)
 VALUES (
        'Is Blockchain Technology the New Internet ?',
        NOW(),
        'The blockchain is an undeniably ingenious invention – the brainchild of a person or group of people known by the pseudonym, Satoshi Nakamoto. But since then, it has evolved into something greater, and the main question every single person is asking is: What is Blockchain?
        By allowing digital information to be distributed but not copied, blockchain technology created the backbone of a new type of internet.' ,
        'Maroua '
 );
 INSERT INTO article (

        title_article,
        date_article,
        content_article,
        author_article
)
 VALUES (
        'Is Blockchain Technology the New Internet ?',
        NOW(),
        'The blockchain is an undeniably ingenious invention – the brainchild of a person or group of people known by the pseudonym, Satoshi Nakamoto. But since then, it has evolved into something greater, and the main question every single person is asking is: What is Blockchain?
        By allowing digital information to be distributed but not copied, blockchain technology created the backbone of a new type of internet. Originally devised for the digital currency, Bitcoin, (Buy Bitcoin) the tech community has now found other potential uses for the technology.',
        'Maroua '
 );
 
  INSERT INTO home (

        id_home,
        title_home,
        img_home,
        content_home
)
 VALUES (
        'home',
        'another test',
        'https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fwp-content%2Fblogs.dir%2F6%2Ffiles%2F2019%2F09%2Fblackpink-rose-unistella-park-eunkyung-korean-nail-artist-ysl-monogram-design-1.jpg?q=75&w=800&cbr=1&fit=max',
        'Blackpink Rosé at a Saint Laurent event'
 );



  






